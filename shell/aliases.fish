alias gs="git status"
alias mv="mv -i"
alias ll="ls -al"

function mkcd 
  mkdir -p "$1" && cd "$1"
end

## Misc
alias my-ip="curl myip.ipip.net"
alias www='https_proxy="http://127.0.0.1:${MY_PROXY_PORT}" http_proxy="http://127.0.0.1:${MY_PROXY_PORT}"'
