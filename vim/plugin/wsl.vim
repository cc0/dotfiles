" 
" Vim autoloads scripts in the .vim/plugin directory
" https://learnvimscriptthehardway.stevelosh.com/chapters/42.html
"

" WSL yank support
let uname = substitute(system('uname'),'\n','','')
if uname == 'Linux'
    let lines = readfile("/proc/version")
    if lines[0] =~ "Microsoft"

        " WSL-specific color settings
        set term=screen-256color " this plus extra tmux settings to fix color conflicts in tmux
                                 " the following line enforces a degraded color mode to fix color conflicts in WSL Ubuntu
                                 " the benefit is that your Vim will look exactly the same everywhere
                                 " and the downside is that it changes the overall color of the theme...
        let g:solarized_termcolors=256 
        
        " Enable WSL-specific clipboard settings

        let s:clip = '/mnt/c/Windows/System32/clip.exe'
        if executable(s:clip)
            augroup WSLYank
                autocmd!
                autocmd TextYankPost * if v:event.operator ==# 'y' | call system(s:clip, @0) | endif
            augroup END
        endif

        " toggle PASTE mode
        " PASTE mode: disable auto indenting for pasted content
        set pastetoggle=<F3>
        
    endif
endif
