" trying to be explicit and safe
set nocompatible " not vi compatible, should have been enabled by default

" directory
" set the directory of the last opened file as the current directory
set autochdir 

" encoding
set fileencodings=utf-8,gb2312,gb18030,gbk,ucs-bom,cp936,latin1

" indentation
" smartindent doesnt work with filetype based indentation
" smartindent itself is quite similar to filetype based indentation
" but the latter usually works better according to :h 'smartindent'
set autoindent
filetype plugin indent on " better indentation
set tabstop=4
set shiftwidth=4    " 8 by default
set softtabstop=4   " delete spaces as if they were tabs
set expandtab
set backspace=indent,eol,start

" disable deeping
set noerrorbells visualbell t_vb=

" show extra info
set number	
set ruler " show column number
set relativenumber
set showmatch
set laststatus=2 " always show status line
set scrolloff=5 " show at least five lines above and below current line when possible

" searching
set hlsearch
set smartcase
set ignorecase
set smartcase " do not ignore case if there is upper case characters
set incsearch

" colors
"set term=screen-256color " this plus extra tmux setting to fix color conflicts in tmux
" the following line enforces a degraded color mode to fix color conflicts in WSL Ubuntu
" the benefit is that your Vim will look exactly the same everywhere
" and the downside is that it changes the overall color of the theme...
 let g:solarized_termcolors=256 
syntax enable
"set background=dark
"color solarized

" enable mouse
set mouse+=a

" diable unused keybindings
nmap Q <Nop>

" remap nohighlight toggling
" used to be nnoremap <ESC> :noh<CR><ESC>
" but using <ESC> can cause strage problems
" it's better to use a less popular key to avoid conflicts
nnoremap \\ :noh<CR><ESC>

" disabled arrow keys in normal mode
nnoremap <Left>  :echoe "Use h"<CR>
nnoremap <Right> :echoe "Use l"<CR>
nnoremap <Up>    :echoe "Use k"<CR>
nnoremap <Down>  :echoe "Use j"<CR>
" disabled arrow keys in insert mode
inoremap <Left>  <ESC>:echoe "Use h"<CR>
inoremap <Right> <ESC>:echoe "Use l"<CR>
inoremap <Up>    <ESC>:echoe "Use k"<CR>
inoremap <Down>  <ESC>:echoe "Use j"<CR>

" Use Ctrl-HJKL to switch to another window
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" NEOVIM Only
set guicursor=
set clipboard=unnamedplus " always use system clipboard
